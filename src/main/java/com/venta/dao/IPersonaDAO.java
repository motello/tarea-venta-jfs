package com.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.venta.model.Persona;

public interface IPersonaDAO extends JpaRepository<Persona, Integer>{

}
