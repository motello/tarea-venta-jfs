package com.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.venta.model.Producto;

public interface IProductoDAO extends JpaRepository<Producto, Integer>{

}
