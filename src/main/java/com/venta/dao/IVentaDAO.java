package com.venta.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.venta.model.Venta;

public interface IVentaDAO extends JpaRepository<Venta, Integer>{

}
