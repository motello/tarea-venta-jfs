package com.venta.service;

import java.util.List;

public interface ICRUD<T> {

	T registrar(T t); // IGUAL QUE: Paciente registrar(Paciente pac);
	
	T modificar(T t);
	
	void eliminar(int id);
	
	T listarId(int id);
	
	List<T> listar();
}
