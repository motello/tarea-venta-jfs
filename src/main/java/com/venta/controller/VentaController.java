package com.venta.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


import com.venta.model.Venta;
import com.venta.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {
	
	
	@Autowired
	private IVentaService service;

	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public ResponseEntity<List<Venta>> listar(){ //clase rconsonseentity puedo manipular el estatus code del servicio, es un conjunto de numeros ok es 200
		List<Venta> Ventas = new ArrayList<>();
		Ventas = service.listar();
		return new ResponseEntity<List<Venta>>(Ventas, HttpStatus.OK); // es el 200
	}
	
	@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>registrar(@Valid @RequestBody Venta Ventas){ //valid que rconsete los constraint que definimos en la Entity // recivo los datos que seleccion en ConlsutaDTO, desde la vista solo esos dos 
		Venta ven = new Venta();
		ven = service.registrar(Ventas); //le paso lo que viene despues del RequestBody
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ven.getIdVenta()).toUri(); //definir la ruta que genera el recurso de Consulta
		return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
	}
	
	
	
	
}
