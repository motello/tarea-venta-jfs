package com.venta.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.venta.exception.ModeloNotFoundException;
import com.venta.model.Producto;
import com.venta.service.IProductoService;



@RestController
@RequestMapping("/productos") 
public class ProductoController {

	@Autowired
	private IProductoService service; 
	
	
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Producto>> listar(){ 
		List<Producto> producto = new ArrayList<>();
		producto = service.listar();
		return new ResponseEntity<List<Producto>>(producto, HttpStatus.OK); 
	}
	

	@GetMapping(value = "/{id}")
	public Resource<Producto> listarId(@PathVariable("id") Integer id) {
		Producto pro = service.listarId(id);
		if (pro ==null) {
			throw new ModeloNotFoundException("ID: " + id); 
		} 
		
	//implementar el HATEOAS
		
		Resource<Producto> resource = new Resource<Producto>(pro);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("proiente-resource"));
		
		
		return resource;
		
	}
	
	
	@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>registrar(@Valid @RequestBody Producto proiente){ //valid que rproete los constraint que definimos en la Entity
		Producto pro = new Producto();
		pro = service.registrar(proiente); //le paso lo que viene dproues del RequestBody
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pro.getIdProducto()).toUri(); //definir la ruta que genera el recurso de proiente
		return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
	}
	
	
	@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>actualiza(@Valid @RequestBody Producto proiente){ //valid que rproete los constraint que definimos en la Entity
		Producto pro = new Producto();
		pro = service.modificar(proiente); //le paso lo que viene dproues del RequestBody
		return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
	Producto pro = service.listarId(id);
	if (pro ==null) {
		throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
	} else {
		service.eliminar(id);
		
	}
	}
	
	
	
}
