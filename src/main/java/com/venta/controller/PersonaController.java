package com.venta.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.venta.exception.ModeloNotFoundException;
import com.venta.model.Persona;
import com.venta.service.IPersonaService;



@RestController
@RequestMapping("/personas") 
public class PersonaController {

	@Autowired
	private IPersonaService service; 
	
	
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<List<Persona>> listar(){ 
		List<Persona> persona = new ArrayList<>();
		persona = service.listar();
		return new ResponseEntity<List<Persona>>(persona, HttpStatus.OK); 
	}
	

	@GetMapping(value = "/{id}")
	public Resource<Persona> listarId(@PathVariable("id") Integer id) {
		Persona per = service.listarId(id);
		if (per ==null) {
			throw new ModeloNotFoundException("ID: " + id); 
		} 
		
	//implementar el HATEOAS
		
		Resource<Persona> resource = new Resource<Persona>(per);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("persona-resource"));
		
		
		return resource;
		
	}
	
	
	@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>registrar(@Valid @RequestBody Persona persona){ //valid que rperete los constraint que definimos en la Entity
		Persona per = new Persona();
		per = service.registrar(persona); //le paso lo que viene dperues del RequestBody
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(per.getIdPersona()).toUri(); //definir la ruta que genera el recurso de persona
		return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
	}
	
	
	@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>actualiza(@Valid @RequestBody Persona persona){ //valid que rperete los constraint que definimos en la Entity
		Persona per = new Persona();
		per = service.modificar(persona); //le paso lo que viene dperues del RequestBody
		return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
	Persona per = service.listarId(id);
	if (per ==null) {
		throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
	} else {
		service.eliminar(id);
		
	}
	}
	
	
	
}
